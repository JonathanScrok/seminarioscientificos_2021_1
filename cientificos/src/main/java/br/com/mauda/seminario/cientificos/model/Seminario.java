package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Seminario {

    private Long id;
    private String titulo;
    private String descricao;

    private Boolean mesaRedonda;
    private Integer qtdInscricoes;

    private List<AreaCientifica> areasCientificas = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();

    LocalDate semiDate = LocalDate.now();

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdeInscricoes) {
        this.areasCientificas.add(areaCientifica);
        this.professores.add(professor);
        professor.adicionarSeminario(this);
        this.qtdInscricoes = qtdeInscricoes;
        for (int i = 0; i < qtdeInscricoes; i++) {
            new Inscricao(this);
        }
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public void adicionarAreaCientifica(AreaCientifica area) {
        this.areasCientificas.add(area);
    }

    public void adicionarProfessor(Professor professor) {
        this.professores.add(professor);
    }

    public boolean possuiAreaCientifica(AreaCientifica area) {
        return this.areasCientificas.contains(area);
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public boolean possuiProfessor(Professor professor) {
        return this.professores.contains(professor);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public Object getData() {
        // TODO Auto-generated method stub
        return this.semiDate;
    }

}

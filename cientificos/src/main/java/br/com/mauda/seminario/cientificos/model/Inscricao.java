package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private Seminario seminario;
    private Estudante estudante;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    LocalDateTime inscDateTime = LocalDateTime.now();

    public Inscricao(Seminario seminario) {
        seminario.adicionarInscricao(this);
        this.seminario = seminario;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.estudante = estudante;
        this.estudante.adicionarInscricao(this);
        this.direitoMaterial = direitoMaterial;
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }

    public Object getDataCompra() {
        // TODO Auto-generated method stub
        return null;
    }

    public Object getDataCheckIn() {
        // TODO Auto-generated method stub
        return null;
    }

    public ChronoLocalDateTime<?> getDataCriacao() {
        // TODO Auto-generated method stub
        return this.inscDateTime;
    }

}
